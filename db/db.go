package db

import (
	"github.com/couchbaselabs/go-couchbase"
	"log"
	"os"
	"fmt"
)

func New() *couchbase.Bucket {
	cbUri := os.Getenv("COUCHBASE_URI")
	c, err := couchbase.Connect(cbUri)
	if err != nil {
		log.Printf("Error connecting to couchbase at %v with error %v", cbUri, err)
		return nil
	}

	poolname := "default"
	pool, err := c.GetPool(poolname)
	if err != nil {
		log.Printf("Error getting couchbase pool %v:  %v", poolname, err)
		return nil
	}

	cbBucket := os.Getenv("COUCHBASE_BUCKET")
	bucket, err := pool.GetBucket(cbBucket)
	if err != nil {
		log.Printf("Error getting couchbase bucket %v:  %v", cbBucket, err)
		return nil
	}

	return bucket
}

func Save(b *couchbase.Bucket, s Storable) error {
	fmt.Printf("%#v\n", s)
	return b.Set(s.Id(), 0, CbMarshal(s))
}

func Fetch(b *couchbase.Bucket, key string, s Storable) error {
	v := ""
	b.Get(key, v)
	return CbUnmarshal(v, s)
}
