package db

import (
	"encoding/json"
	"fmt"
)

type cbMarshalMeta struct {
	S Storable // actual data
	T string   // type of data
}

type cbMarshalMetaRaw struct {
	S json.RawMessage
	T string
}

// CbMarshal turns Storables into marshaled strings, usually json with metadata
func CbMarshal(s Storable) interface{} {
	v := cbMarshalMeta{
		S: s,
		T: s.TypeId(),
	}
	return v
}

// CbUnmarshal is the inverse of CbMarshal
func CbUnmarshal(s string, st Storable) error {
	v := cbMarshalMetaRaw{}
	json.Unmarshal([]byte(s), &v)
	fmt.Println("CbUnmarshal", s, " -> ", v)

	return json.Unmarshal(v.S, &st)
}
