package db

import (
	"errors"
	"fmt"
	"reflect"
)

var (
	ErrDuplicateStorableTypeId = errors.New("ErrDuplicateStorableTypeId")
)

type Storable interface {
	Id() string
	TypeId() string
}

type Marshaler interface {
	MarshalJSON() ([]byte, error)
}

var types = make(map[string]reflect.Type)

// TODO: remove: this function is useless, right?
func Register(v Storable) bool {
	if v.TypeId() == "" {
		panic(fmt.Sprintf("v %v has empty TypeId()", v))
	}
	val, found := types[v.TypeId()]
	if !found {
		types[v.TypeId()] = reflect.TypeOf(v)
		return true
	} else if val != reflect.TypeOf(v) {
		panic(fmt.Sprintf("%v: %v; %v != %v", ErrDuplicateStorableTypeId, v, val, reflect.TypeOf(v)))
	}
	return false
}

// userid generated server-side when account is created
// other ids generated client-side when first connecting to a user account

// user 1<-->* openid
// daemon *<-->1 user

// for storage, can we save types just like marshallers do it, as in calling myDb.Register(User) ?

// TODO: where are id's generated? How do we map them? Does each daemon have its own id?
