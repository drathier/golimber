package db_test

import (
	"github.com/drathier/golimber/db"
	"testing"
)

type reg1 struct{}
type reg2 struct{}

func (reg1) TypeId() string {
	return "test-reg1"
}

func (reg2) TypeId() string {
	return "test-reg2"
}

func (reg1) Id() string {
	return "test-reg1-id"
}

func (reg2) Id() string {
	return "test-reg1-id"
}

func TestRegister(t *testing.T) {
	if db.Register(reg1{}) == false {
		t.Fail()
	}
	if db.Register(reg2{}) == false {
		t.Fail()
	}
	if db.Register(reg1{}) == true {
		t.Fail()
	}
	if db.Register(reg2{}) == true {
		t.Fail()
	}
	if db.Register(reg2{}) == true {
		t.Fail()
	}
	if db.Register(reg1{}) == true {
		t.Fail()
	}
}
