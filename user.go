package main

import (
	"fmt"
	"github.com/markbates/goth"
)

type User struct {
	RawData           map[string]interface{}
	Email             string
	Name              string
	NickName          string
	Description       string
	UserID            string
	AvatarURL         string
	Location          string
	AccessToken       string
	AccessTokenSecret string
	Source            Source
}

func NewUser() User {
	u := User{
		RawData: make(map[string]interface{}),
	}
	return u
}

func FromGoth(g goth.User, s Source) User {
	u := NewUser()
	for k, v := range g.RawData {
		u.RawData[k] = v
	}
	u.Email = g.Email
	u.Name = g.Name
	u.NickName = g.NickName
	u.Description = g.Description
	u.UserID = g.UserID
	u.AvatarURL = g.AvatarURL
	u.Location = g.Location
	u.AccessToken = g.AccessToken
	u.AccessTokenSecret = g.AccessTokenSecret
	u.Source = s
	fmt.Println("fromgoth", u)
	return u
}

func (u *User) Goth() goth.User {
	g := goth.User{}
	for k, v := range u.RawData {
		g.RawData[k] = v
	}
	g.Email = u.Email
	g.Name = u.Name
	g.NickName = u.NickName
	g.Description = u.Description
	g.UserID = u.UserID
	g.AvatarURL = u.AvatarURL
	g.Location = u.Location
	g.AccessToken = u.AccessToken
	g.AccessTokenSecret = u.AccessTokenSecret
	return g
}

func (u *User) Id() string {
	return fmt.Sprintf("%v_%v", u.Source, u.UserID)
}

func (u *User) TypeId() string {
	return "User"
}

//go:generate stringer -type=Source
type Source int

const (
	UnknownSource Source = iota
	Twitter
	Facebook
	Gplus
	Spotify
	Linkedin
	Github
	Lastfm
)

var srcMap = map[string]Source{
	"Twitter":  Twitter,
	"Facebook": Facebook,
	"Gplus":    Gplus,
	"Spotify":  Spotify,
	"Linkedin": Linkedin,
	"Github":   Github,
	"Lastfm":   Lastfm,
}

func NewSource(s string) Source {
	v := srcMap[s]
	fmt.Println("source", s, "->", v)
	if v == UnknownSource {
		fmt.Errorf("UnknownSource %v\n", s)
	}
	return v
}
